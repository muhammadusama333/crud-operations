<!DOCTYPE html>
<html>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for all buttons */
button {
  background-color: #04AA6D;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

button:hover {
  opacity:1;
}

/* Extra styles for the cancel button */
.loadbtn{
padding: 14px 20px;
  background-color: #3690ff;
}
.save{
  padding: 14px 20px;
  background-color: #4930ff;
  cursor: pointer;
}
.update{
padding: 14px 20px;
  background-color: #4930ff;
  cursor: pointer;
}
.close{
  padding: 14px 20px;
  background-color: #9122ff;
  cursor: pointer;
}
.cancelbtn {
  padding: 14px 20px;
  background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn, .signupbtn {
  float: left;
  width: 50%;
}

/* Add padding to container elements */
.container {
  padding: 16px;
}

/* Clear floats */
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}


 #modal {
    background-color: whitesmoke;
    padding: 6%;
    position: absolute;
    width: 36%;
    left: 30%;
    top: 24%;
    display: none;
}

/* Change styles for cancel button and signup button on extra small screens */
@media screen and (max-width: 300px) {
  .cancelbtn, .signupbtn {
     width: 100%;
  }
}
</style>
<body>
<table border='1px solid black' id="tableid">

</table>


<form style="border:1px solid #ccc">
 

  <div class="container">
    <h1>Sign Up</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email"  id="mail" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" id="passw" required>

    <label for="pswrepeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" id="passwrep" required>
    
    <label>
      <input type="checkbox" checked="checked" name="remember" style="margin-bottom:15px"> Remember me
    </label>
    
    <p>By creating an account you agree to our <a href="#" style="color:dodgerblue">Terms & Privacy</a>.</p>

    <div class="clearfix">
      <button type="button" class="loadbtn" id="load">Load</button>
      <button type="submit" class="signupbtn" id="bt1">Sign Up</button>
    </div>
  </div>
</form>


<div id="modal">


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script type="text/javascript">




$(document).ready(function(){





function loadtable()
{
   $.ajax({
      url:'Table.php',
      type:'POST',
      success:function(data)
      {
     $("#tableid").html(data);
      }
    });
}




$(document).on("click",".cancelbtn",function()
{
var sid = $(this).data('id');

$.ajax({
  url : 'del.php',
  type : 'POST',
  data : {id:sid},
  success:function(data)
  {
    alert(data);
    loadtable();
  }
});



});


$(document).on("click",".update",function()
  {
var sid = $(this).data('id');

$.ajax({
 url : 'uptodate.php',
 type : 'POST',
 data : {id:sid},
 success:function(data)
 {

  $("#modal").html(data);
  $("#modal").show(); 
  $("#close1").on("click",function()
{
$("#modal").hide();
});
 }
});

  });




$('#load').on("click",function()
  {
   loadtable();
 
  });




  $("#bt1").on("click",function(e){
    e.preventDefault();
    var email = $("#mail").val();
    var pass = $("#passw").val();
    var repass = $("#passwrep").val();

    $.ajax({
      url:"Insert.php",
      type : "POST",
      data :{emails:email,password:pass,repassword:repass},
      success:function(data)
      {
        alert(data);
      }
    });
  });


$(document).on("click","#save1",function()
{

var id = $("#sid").val();
var email = $("#mail2").val();
var pass = $("#passw2").val();
var repass = $("#passwrep2").val();

$.ajax({
  url:"Update.php",
  type:"POST",
  data:{sid:id,mail:email,password:pass,repassword:repass},
  success:function(data)
  {
    alert(data);
    $("#modal").hide();
    loadtable();
  }
});
});


});
</script>




</body>
</html>
